'use strict';
const decimal = require('decimal.js');

const prefixes = {
	'Y':  [ 'yotta',  24 ],
	'Z':  [ 'zetta',  21 ],
	'E':  [ 'exa',    18 ],
	'P':  [ 'peta',   15 ],
	'T':  [ 'tera',   12 ],
	'G':  [ 'giga',   9  ],
	'M':  [ 'mega',   6  ],
	'k':  [ 'kilo',   3  ],
	'h':  [ 'hecto',  2  ],
//	'da': [ 'deca',   1  ], (causes too much noise in toBestPrefix. could special case it but I'm not sure anyone really wants this prefix anyway)
	'':   [ '',       0  ],
	'd':  [ 'deci',  -1  ],
	'c':  [ 'centi', -2  ],
	'm':  [ 'milli', -3  ],
	'μ':  [ 'micro', -6  ],
	'n':  [ 'nano',  -9  ],
	'p':  [ 'pico',  -12 ],
	'f':  [ 'femto', -15 ],
	'a':  [ 'atto',  -18 ],
	'z':  [ 'zepto', -21 ],
	'y':  [ 'yocto', -24 ]
};
const pfx = {};
Object.entries(prefixes).forEach(([ sym, [ _name, exp ] ]) =>
	pfx[sym] = decimal(10).pow(exp)
);

const converter = module.exports = (defs) => {
	const unitToDomain = {};
	Object.entries(defs).forEach(([ domain, units ]) => {
		Object.keys(units).forEach((unit) => {
			if (unit === 'anchor') {
				return;
			}
			if (!unitToDomain[unit]) {
				unitToDomain[unit] = [];
			}
			unitToDomain[unit].push(unit);
		});
		units[units.anchor] = {
			'to-anchor': (x) => x,
			'from-anchor': (x) => x
		}
	});

	return {
		'from': (amount, from) => {
			const fromUnit = toUnit(from);
			const tbl = defs[fromUnit.domain];
			// apply prefix where applicable
			const decAmount = decimal(amount)
				.times(pfx[fromUnit.prefix]);

			const rv = {
				'to': (dest) => {
					const destUnit = toUnit(dest);
					// convert to anchor
					const asAnchor = tbl[fromUnit.unit]['to-anchor'](decAmount);
					// convert anchor to destination unit
					return tbl[destUnit.unit]['from-anchor'](asAnchor)
						// use supplied prefix where applicable
						.dividedBy(pfx[destUnit.prefix]);
				},
				'toBestPrefix': (dest) => {
					const convBase = rv.to([ fromUnit.domain, '', dest ]);
					let prefix = null;
					Object.entries(prefixes).some(([ sym, [ _name, exp ] ]) => {
						prefix = sym;
						// not sure if this is the best heuristic but it works in my tests so far
						if (exp <= convBase.e) {
							return true;
						}
					});
					return { prefix, 'amount': convBase.dividedBy(pfx[prefix]) };
				}
			};
			return rv;
		},
		'getUnits': (type) => Object.keys(defs[type])
			.filter((k) => k !== 'anchor')
			.sort(),
		'getDomain': (unit) => unitToDomain[unit],
		'getPrefixes': () => prefixes
	};
};

// allow either { domain, prefix, unit } or [ domain, prefix, unit ], normalizing to former
function toUnit(parts) {
	if (Array.isArray(parts)) {
		return {
			'domain': parts[0],
			'prefix': parts[1],
			'unit': parts[2]
		};
	}
	return parts;
}

/*
const conv = converter(require('./defs/bundles/all'));
console.log(
	conv.from(1, [ 'energy', '', 'J' ]).to([ 'energy', '', 'eV' ]).toNumber()
);
const t = conv.from(1, [ 'energy', '', 'J' ]).toBestPrefix('eV');
t.amount = t.amount.toNumber();
console.log(t);

console.log(conv.getUnits('temperature'));
console.log(
	conv.from(1, [ 'temperature', 'k', 'F' ]).to([ 'temperature', '', 'F' ]).toNumber() === 1000,
	conv.from(1, [ 'temperature', 'k', 'F' ]).to([ 'temperature', 'k', 'F' ]).toNumber() === 1,
	conv.from(1, [ 'temperature', 'da', 'F' ]).to([ 'temperature', '', 'F' ]).toNumber() === 10,
	conv.from(1, [ 'temperature', '', 'F' ]).to([ 'temperature', 'da', 'F' ]).toNumber() === 0.1,
);

const t = conv.from(1500, [ 'temperature', 'k', 'F' ]).toBestPrefix('F');
t.amount = t.amount.toNumber();
console.log(t);

*/
