'use strict';
module.exports = {
	'anchor': 'C',
	'K': {
		'to-anchor':   (K) => K.minus('273.15'),
		'from-anchor': (C) => C.plus('273.15')
	},
	'F': {
		'to-anchor':   (F) => F.minus(32).times(5).dividedBy(9),
		'from-anchor': (C) => C.times(9).dividedBy(5).plus(32)
	},
	'R': {
		'to-anchor':   (R) => R.minus('491.67').times(5).dividedBy(9),
		'from-anchor': (C) => C.times(9).dividedBy(5).plus('491.67')
	}
}
