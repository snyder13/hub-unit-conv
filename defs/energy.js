'use strict';
module.exports = {
	'anchor': 'J',
	'eV': {
		'to-anchor':   (eV) => eV.times('1.602176565e-19'),
		'from-anchor': (J) => J.dividedBy('1.602176565e-19')
	}
};
