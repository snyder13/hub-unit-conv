'use strict';
module.exports = {};
[ 'energy', 'temperature' ].forEach((domain) =>
	module.exports[domain] = require(`${ __dirname }/../${ domain }.js`)
);
